cmake_minimum_required (VERSION 3.5)
project (2dengine)
set(CMAKE_CXX_STANDARD 11)

set(SOURCE_DOMAIN ./src)
set(HEADER_DOMAIN ./headers)

include_directories(${SOURCE_DOMAIN})
include_directories(${HEADER_DOMAIN})

include_directories(/usr/include/allegro)
link_directories(/usr/lib/allegro/4.4.2)

set(
        SOURCE_FILES
        ${SOURCE_DOMAIN}/main.cpp
        ${SOURCE_DOMAIN}/Engine.cpp
        ${SOURCE_DOMAIN}/LineSegment.cpp
        ${SOURCE_DOMAIN}/Triangle.cpp
        ${SOURCE_DOMAIN}/Point2D.cpp
        ${SOURCE_DOMAIN}/Color.cpp
        ${SOURCE_DOMAIN}/Scene.cpp
        ${SOURCE_DOMAIN}/Keyboard.cpp
        ${SOURCE_DOMAIN}/Primitive.cpp
        ${SOURCE_DOMAIN}/Viewport.cpp
        ${SOURCE_DOMAIN}/Rectangle.cpp
        ${SOURCE_DOMAIN}/Circle.cpp
        ${SOURCE_DOMAIN}/BoundingBox.cpp
        ${SOURCE_DOMAIN}/Mouse.cpp
        ${SOURCE_DOMAIN}/BitmapHandler.cpp
        ${SOURCE_DOMAIN}/SpriteObject.cpp
        ${SOURCE_DOMAIN}/Polyline.cpp
        ${HEADER_DOMAIN}/Engine.hpp
        ${HEADER_DOMAIN}/Rectangle.hpp
        ${HEADER_DOMAIN}/header.hpp
        ${HEADER_DOMAIN}/InitBitmask.hpp
        ${HEADER_DOMAIN}/Point2D.hpp
        ${HEADER_DOMAIN}/Color.hpp
        ${HEADER_DOMAIN}/LineSegment.hpp
        ${HEADER_DOMAIN}/Triangle.hpp
        ${HEADER_DOMAIN}/Scene.hpp
        ${HEADER_DOMAIN}/Keyboard.hpp
        ${HEADER_DOMAIN}/Primitive.hpp
        ${HEADER_DOMAIN}/Viewport.hpp
        ${HEADER_DOMAIN}/ColorType.hpp
        ${HEADER_DOMAIN}/Circle.hpp
        ${HEADER_DOMAIN}/CircleType.hpp
        ${HEADER_DOMAIN}/Mouse.hpp
        ${HEADER_DOMAIN}/MouseType.hpp
        ${HEADER_DOMAIN}/BitmapHandler.hpp
        ${HEADER_DOMAIN}/SpriteObject.hpp
        ${HEADER_DOMAIN}/Polyline.hpp
        ${HEADER_DOMAIN}/BoundingBox.hpp )
add_executable(2dengine ${SOURCE_FILES})

set(COMPILE_FLAGS "-I/usr/include -L/usr/lib -lalleg -O2")
set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${COMPILE_FLAGS}")
