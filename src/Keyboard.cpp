#include <allegro.h>
#include "Keyboard.hpp"

int Keyboard::handle() {
    int key = readkey();
    return key & MASK;
}

bool Keyboard::isEsc() {
    return key[KEY_ESC];
}

bool Keyboard::isRightArrow() {
    return key[KEY_RIGHT];
}

bool Keyboard::isLeftArrow() {
    return key[KEY_LEFT];
}

bool Keyboard::isUpArrow() {
    return key[KEY_UP];
}

bool Keyboard::isDownArrow() {
    return key[KEY_DOWN];
}

bool Keyboard::isOne() {
    return key[KEY_1] || key[KEY_1_PAD];
}

bool Keyboard::isTwo() {
    return key[KEY_2] || key[KEY_2_PAD];
}

bool Keyboard::isThree() {
    return key[KEY_3] || key[KEY_3_PAD];
}

bool Keyboard::isFour() {
    return key[KEY_4] || key[KEY_4_PAD];
}

bool Keyboard::isFive() {
    return key[KEY_5] || key[KEY_5_PAD];
}

bool Keyboard::isSix() {
    return key[KEY_6] || key[KEY_6_PAD];
}

bool Keyboard::isSeven() {
    return key[KEY_7] || key[KEY_7_PAD];
}

bool Keyboard::isT() {
    return key[KEY_T];
}

bool Keyboard::isL() {
    return key[KEY_L];
}

bool Keyboard::isR() {
    return key[KEY_R];
}

bool Keyboard::isF() {
    return key[KEY_F];
}

bool Keyboard::isO() {
    return key[KEY_O];
}

bool Keyboard::isC() {
    return key[KEY_C];
}

bool Keyboard::isQ() {
    return key[KEY_Q];
}

bool Keyboard::isBackspace() {
    return key[KEY_BACKSPACE];
}

bool Keyboard::isB() {
    return key[KEY_B];
}

bool Keyboard::isComma() {
    return key[KEY_COMMA];
}

bool Keyboard::isDot() {
    return key[KEY_STOP];
}

bool Keyboard::isPlus() {
    return key[KEY_EQUALS];
}

bool Keyboard::isMinus() {
    return key[KEY_MINUS];
}

bool Keyboard::isV() {
    return key[KEY_V];
}

bool Keyboard::isH() {
    return key[KEY_H];
}

bool Keyboard::isX() {
    return key[KEY_X];
}
