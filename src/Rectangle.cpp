#include <Engine.hpp>
#include <Rectangle.hpp>

Rectangle::Rectangle(Point2D *a,Point2D *b)
    : Primitive(), a(a), b(b) {
    this->boundingBox = new BoundingBox(a, b);
}

void Rectangle::draw(BITMAP* bitmap) {
    rect(bitmap, a->getX(), a->getY(), b->getX(), b->getY(), color.getColor());
}

BoundingBox *Rectangle::getBoundingBox() {
    return this->boundingBox;
}

Point2D *Rectangle::getPointA() {
    return this->a;
}

Point2D *Rectangle::getPointB() {
    return this->b;
}

void Rectangle::shift(Point2D point2D) {
    a->shift(point2D);
    b->shift(point2D);
}

void Rectangle::rotate(double angle) {
    Point2D center(screen->w / 2, screen->h / 2);
    a->rotate(angle, center);
    b->rotate(angle, center);
}

void Rectangle::scale(double factor) {
    Point2D center(b->getX() - (b->getX() - a->getX()) / 2, b->getY() - (b->getY() - b->getX()) / 2);
    a->scale(factor, center);
    b->scale(factor, center);
}

