#include <LineSegment.hpp>
#include <Engine.hpp>
#include <algorithm>
LineSegment::LineSegment(Point2D *a, Point2D *b)
    : Primitive(), a(a), b(b) {}


void LineSegment::draw(BITMAP* bitmap) {
    line(bitmap, a->getX(), a->getY(), b->getX(), b->getY(), color.getColor());
}

Point2D * LineSegment::getPointA() {
    return this->a;
}

Point2D* LineSegment::getPointB() {
    return this->b;
}

void LineSegment::swapPoints(){
    std::swap(this->a,this->b);
}

std::vector<LineSegment> LineSegment::getRandomLines(int n) {
    std::vector<LineSegment> lines;

    for(int i = 0; i < n; ++i) {
        lines.emplace_back(LineSegment(new Point2D(Engine::getInstance()->getRandomX(), Engine::getInstance()->getRandomY()),
        new Point2D(Engine::getInstance()->getRandomX(), Engine::getInstance()->getRandomY())));
    }

    return lines;
}

LineSegment *LineSegment::clone() {
    return new LineSegment(a, b);
}



