#include <Triangle.hpp>
#include <Scene.hpp>
#include <list>
#include <Viewport.hpp>
#include <Circle.hpp>
#include <Rectangle.hpp>
#include <SpriteObject.hpp>
#include "Polyline.hpp"


void Scene::drawIncrementalAlgorithm() {
    engine->doubleBufferClear();
    auto *lineSegment = new LineSegment(new Point2D(Engine::getInstance()->getRandomX(), Engine::getInstance()->getRandomY() ),
                                        new Point2D(Engine::getInstance()->getRandomX(), Engine::getInstance()->getRandomY()));
    float deltaX = lineSegment->getPointB()->getX() - lineSegment->getPointA()->getX();
    float deltaY = lineSegment->getPointB()->getY() - lineSegment->getPointA()->getY();
    float tilt = deltaY / deltaX;
    if(abs(tilt)<1){
        if(lineSegment->getPointA()->getX()>lineSegment->getPointB()->getX())
            lineSegment->swapPoints();
        if(deltaX==0){
            int x=lineSegment->getPointA()->getX();
            for(int y = lineSegment->getPointA()->getY(); y <= lineSegment->getPointB()->getY(); y++){
                putpixel(engine->getBitmap(), x, y, lineSegment->getColor());
                if(x==0||y==0||x==Engine::MAX_X||y==Engine::MAX_Y)
                    break;
            }
        }
        else{
            float deltaX = lineSegment->getPointB()->getX() - lineSegment->getPointA()->getX();
            float deltaY = lineSegment->getPointB()->getY() - lineSegment->getPointA()->getY();
            float tilt = deltaY / deltaX;
            int y = lineSegment->getPointA()->getY();
            for(int x = lineSegment->getPointA()->getX(); x <= lineSegment->getPointB()->getX(); x++) {
                putpixel(engine->getBitmap(), x, int(y + 0.5), lineSegment->getColor());
                y += tilt;
                if(x==0||y==0||x==Engine::MAX_X||y==Engine::MAX_Y)
                    break;
            }
        }
    }
    else{
        if(lineSegment->getPointA()->getY()>lineSegment->getPointB()->getY())
            lineSegment->swapPoints();
        if(deltaX==0){
            int x=lineSegment->getPointA()->getX();
            for(int y = lineSegment->getPointA()->getY(); y <= lineSegment->getPointB()->getY(); y++){
                putpixel(engine->getBitmap(), x, y, lineSegment->getColor());
                if(x==0||y==0||x==Engine::MAX_X||y==Engine::MAX_Y)
                    break;
            }
        }
        else{
            float deltaX = lineSegment->getPointB()->getX() - lineSegment->getPointA()->getX();
            float deltaY = lineSegment->getPointB()->getY() - lineSegment->getPointA()->getY();
            float tilt = deltaX / deltaY;
            int x=lineSegment->getPointA()->getX();
            for(int y = lineSegment->getPointA()->getY(); y <= lineSegment->getPointB()->getY(); y++) {
                putpixel(engine->getBitmap(), int(x + 0.5), y, lineSegment->getColor());
                x += tilt;
                if(x==0||y==0||x==Engine::MAX_X||y==Engine::MAX_Y)
                    break;
            }
        }
    }

    while(true){
        if(engine->getKeyboard()->isQ()) {
            break;
        }
        Engine::getInstance()->doubleBufferDraw();
        rest(Engine::DELAY);
    }

}

void Scene::drawViewport() {
    const int DELTA = 1;
    bool redraw = true;

    auto viewport = new Viewport(new Point2D(50, 50), new Point2D(300, 300));

    std::vector<Point2D> points = Point2D::getRandomPoints(50);
    std::vector<LineSegment> lines = LineSegment::getRandomLines(5);

    while(true) {
        if(redraw) {
            engine->doubleBufferClear();
            viewport->draw(engine->getBitmap());

            for(auto &point : points) {
                point.setColor(Color(viewport->containing(point) ? ColorType::RED : ColorType::GREEN));
                point.draw(engine->getBitmap());
            }

            for(LineSegment &line : lines) {
                std::vector<LineSegment *> intersetcedLines = viewport->intersectLine(&line);
                if(!intersetcedLines.empty()) {
                    for(auto &intersected : intersetcedLines) {
                        if(intersetcedLines.size() == 1) {
                            intersected->setColor(Color(ColorType::GREEN));
                            intersected->draw(engine->getBitmap());
                        }
                        else if(intersetcedLines.size() == 2 && intersected == intersetcedLines[0]) {
                            intersected->setColor(Color(ColorType::GREEN));
                            intersected->draw(engine->getBitmap());
                        }
                        else if(intersetcedLines.size() == 3 && intersected == intersetcedLines[2]) {
                            intersected->setColor(Color(ColorType::GREEN));
                            intersected->draw(engine->getBitmap());
                        }
                        else {
                            intersected->setColor(Color(ColorType::RED));
                            intersected->draw(engine->getBitmap());
                        }

                    }
                }
                else {
                    line.setColor(Color(ColorType::RED));
                    line.draw(engine->getBitmap());
                }
            }

            engine->doubleBufferDraw();
            redraw = false;
        }

        if(engine->getKeyboard()->isQ()) {
            break;
        }
        if(engine->getKeyboard()->isUpArrow()) {
            redraw = true;
            viewport->getPointA()->setY(viewport->getPointA()->getY() - DELTA);
            viewport->getPointB()->setY(viewport->getPointB()->getY() - DELTA);
        }
        if(engine->getKeyboard()->isDownArrow()) {
            redraw = true;
            viewport->getPointA()->setY(viewport->getPointA()->getY() + DELTA);
            viewport->getPointB()->setY(viewport->getPointB()->getY() + DELTA);
        }
        if(engine->getKeyboard()->isRightArrow()) {
            redraw = true;
            viewport->getPointA()->setX(viewport->getPointA()->getX() + DELTA);
            viewport->getPointB()->setX(viewport->getPointB()->getX() + DELTA);
        }
        if(engine->getKeyboard()->isLeftArrow()) {
            redraw = true;
            viewport->getPointA()->setX(viewport->getPointA()->getX() - DELTA);
            viewport->getPointB()->setX(viewport->getPointB()->getX() - DELTA);
        }


        rest(Engine::MILISECONDS);
    }
}

void Scene::drawPrimitives() {
    engine->doubleBufferClear(Color(ColorType::CRIMSON));

    std::list<Primitive*> primitives;

    while(true) {
        if(engine->getKeyboard()->isQ()) {
            break;
        }

        if(engine->getKeyboard()->isT()) {
            Point2D* points[3];

            for(auto &point : points) {
                point = new Point2D(Engine::getInstance()->getRandomX(), Engine::getInstance()->getRandomY());
            }

            Primitive* triangle = new Triangle(points[0], points[1], points[2]);
            triangle->draw(engine->getBitmap());
            primitives.push_back(triangle);
        }
        if(engine->getKeyboard()->isL()) {
            Point2D* points[2];

            for(auto &point : points) {
                point = new Point2D(Engine::getInstance()->getRandomX(), Engine::getInstance()->getRandomY());
            }

            Primitive* lineSegment = new LineSegment(points[0], points[1]);
            lineSegment->draw(engine->getBitmap());
            primitives.push_back(lineSegment);
        }

        if(engine->getKeyboard()->isR()) {
            Point2D* points[2];

            for(auto &point : points) {
                point = new Point2D(Engine::getInstance()->getRandomX(), Engine::getInstance()->getRandomY());
            }

            Primitive* rectangle = new Rectangle(points[0], points[1]);
            rectangle->draw(engine->getBitmap());
            primitives.push_back(rectangle);
        }

        if(engine->getKeyboard()->isX()) {
            engine->doubleBufferClear(Color(ColorType::CRIMSON));
        }


        Engine::getInstance()->doubleBufferDraw();
        rest(Engine::DELAY);
    }

    primitives.clear();
}

void Scene::drawCircles(){
    engine->doubleBufferClear(Color(ColorType::BISQUE));
    std::list<Circle*> circles;
    Color fillColor(ColorType::INDIGO);
    while(true) {
        if(engine->getKeyboard()->isQ()) {
            break;
        }
        if(engine->getKeyboard()->isC()){
            auto* circle = new Circle(100, new Point2D(engine->getRandomX(), engine->getRandomY()));
            circle->draw(engine->getBitmap());
            circles.push_back(circle);
        }
        if(engine->getKeyboard()->isF()){
            auto* circle = new Circle(100, new Point2D(engine->getRandomX(), engine->getRandomY()));
            circle->draw(engine->getBitmap(), QUADRUPLE);
            circles.push_back(circle);
        }
        if(engine->getKeyboard()->isO()){
            auto* circle = new Circle(100, new Point2D(engine->getRandomX(), engine->getRandomY()));
            circle->draw(engine->getBitmap(), OCTAL);
            circles.push_back(circle);
        }
        if(engine->getMouse()->isLeftButton()){
            Point2D point= engine->getMouse()->getPosition();
            for(auto &circle : circles) {
                if(circle->contains(point)){
                    circle->fill(engine->getBitmap(), point,fillColor,Color(ColorType::BISQUE));
                }
            }
        }
        if(engine->getMouse()->isRightButton()){
            auto colorType = std::rand() % (KHAKI-CRIMSON) + CRIMSON;
            fillColor = Color(static_cast<ColorType>(colorType));
        }

        engine->doubleBufferDraw();
        rest(Engine::DELAY);
    }
    circles.clear();
}

void Scene::drawBitmaps(){
    engine->doubleBufferClear(Color(ColorType::CRIMSON));
    auto *spriteObject = new SpriteObject("gfx/duck.bmp", 500, 546);
    bool redraw = true;

    while(true) {
        if(redraw) {
            engine->doubleBufferClear(Color(ColorType::CRIMSON));
            spriteObject->drawSprite();
            engine->doubleBufferDraw();
            redraw = false;
        }

        if(engine->getKeyboard()->isDownArrow()) {
            spriteObject->getPosition()->setY(spriteObject->getPosition()->getY() + 5);
            redraw = true;
        }
        if(engine->getKeyboard()->isUpArrow()) {
            spriteObject->getPosition()->setY(spriteObject->getPosition()->getY() - 5);
            redraw = true;
        }
        if(engine->getKeyboard()->isLeftArrow()) {
            spriteObject->getPosition()->setX(spriteObject->getPosition()->getX() - 5);
            redraw = true;
        }
        if(engine->getKeyboard()->isRightArrow()) {
            spriteObject->getPosition()->setX(spriteObject->getPosition()->getX() + 5);
            redraw = true;
        }
        if(engine->getKeyboard()->isPlus()) {
            engine->doubleBufferClear(Color(ColorType::CRIMSON));
            spriteObject->strechSprite(1.03);
            engine->doubleBufferDraw();
        }
        if(engine->getKeyboard()->isMinus()) {
            engine->doubleBufferClear(Color(ColorType::CRIMSON));
            spriteObject->strechSprite(0.97);
            engine->doubleBufferDraw();
        }
        if(engine->getKeyboard()->isComma()) {
            engine->doubleBufferClear(Color(ColorType::CRIMSON));
            spriteObject->setRotation(-1.0);
            spriteObject->rotateSprite();
            engine->doubleBufferDraw();
        }
        if(engine->getKeyboard()->isDot()) {
            engine->doubleBufferClear(Color(ColorType::CRIMSON));
            spriteObject->setRotation(1.0);
            spriteObject->rotateSprite();
            engine->doubleBufferDraw();
        }
        if(engine->getKeyboard()->isV()) {
            engine->doubleBufferClear(Color(ColorType::CRIMSON));
            spriteObject->verticalFlipSprite();
            engine->doubleBufferDraw();
        }
        if(engine->getKeyboard()->isH()) {
            engine->doubleBufferClear(Color(ColorType::CRIMSON));
            spriteObject->horizontalFlipSprite();
            engine->doubleBufferDraw();
        }
        if(engine->getKeyboard()->isF()) {
            engine->doubleBufferClear(Color(ColorType::CRIMSON));
            spriteObject->flipSprite();
            engine->doubleBufferDraw();
        }
        if(engine->getKeyboard()->isQ()) {
            break;
        }

    }

    delete spriteObject;

}

void Scene::drawPolylines(){
    engine->doubleBufferClear(Color(ColorType::WHITE));
    int numberOfPoints=10;
    int random=2+rand()%numberOfPoints;
    std::vector<Point2D*> points;

    while(true) {
        if(engine->getKeyboard()->isQ()) {
            break;
        }
        if(engine->getKeyboard()->isR()) {
            random=2+rand()%numberOfPoints;
        }
        if(engine->getKeyboard()->isO()) {
            for(int i=0;i<random;i++) {
                points.push_back(new Point2D(Engine::getInstance()->getRandomX(), Engine::getInstance()->getRandomY()));
            }

            Polyline polyline(points);
            polyline.draw(engine->getBitmap());
        }
        if(engine->getKeyboard()->isC()) {
            for(int i=0;i<random;i++) {
                points.push_back(new Point2D(Engine::getInstance()->getRandomX(), Engine::getInstance()->getRandomY()));
            }
            Polyline polyline(points);
            polyline.drawCl(engine->getBitmap());
        }
        Engine::getInstance()->doubleBufferDraw();
        points.clear();
        rest(Engine::DELAY);
    }
}


Scene::Scene(Engine *) {
    this->engine = Engine::getInstance();
}

void Scene::drawBoundingBox() {
    const int DELTA = 2;
    bool redraw = true;
    bool drawBoundingBox = false;
    Color backgroundColor = Color(ColorType::CYAN);

    auto *rectangle = new Rectangle(new Point2D((screen->w / 2) - 50, (screen->h / 2) - 50),
                                     new Point2D((screen->w / 2) + 50, (screen->h / 2) + 50));
    rectangle->setColor(Color(ColorType::BLACK));

    std::vector<Circle*> circles;
    circles.push_back(new Circle(50, new Point2D(static_cast<int>(screen->w * 0.25), static_cast<int>(screen->h * 0.25))));
    circles.push_back(new Circle(50, new Point2D(static_cast<int>(screen->w * 0.75), static_cast<int>(screen->h * 0.25))));
    circles.push_back(new Circle(50, new Point2D(static_cast<int>(screen->w * 0.75), static_cast<int>(screen->h * 0.75))));
    circles.push_back(new Circle(50, new Point2D(static_cast<int>(screen->w * 0.25), static_cast<int>(screen->h * 0.75))));

    for(auto &circle : circles) {
        circle->setColor(Color(ColorType::CRIMSON));
    }

    while(true) {
        if(redraw) {
            engine->doubleBufferClear(backgroundColor);

            rectangle->draw(engine->getBitmap());

            for(auto &circle : circles) {
                circle->draw(engine->getBitmap());
                if (drawBoundingBox) {
                    circle->getBoundingBox()->draw(engine->getBitmap());
                }
            }

            engine->doubleBufferDraw();
            redraw = false;
        }

        if(engine->move(rectangle, DELTA, circles)) {
            redraw = true;
        }

        if(engine->rotate(rectangle)) {
            redraw = true;
        }

        if(engine->scale(rectangle)) {
            redraw = true;
        }

        if(engine->getKeyboard()->isQ()) {
            break;
        }
        if(engine->getKeyboard()->isB()) {
            drawBoundingBox = true;
        }

        rest(Engine::MILISECONDS);
    }
}

