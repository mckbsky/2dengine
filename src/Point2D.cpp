#include <Point2D.hpp>
#include <Engine.hpp>

Point2D::Point2D(int x, int y)
        : Primitive(), x(x), y(y) {}

int Point2D::getX() {
    return this->x;
}

int Point2D::getY() {
    return this->y;
}


void Point2D::draw(BITMAP* bitmap) {
    putpixel(bitmap, x, y, color.getColor());
}

void Point2D::setX(int x) {
    this->x = x;
}

void Point2D::setY(int y) {
    this->y = y;
}

void Point2D::set(int x, int y) {
    this->x = x;
    this->y = y;
}

std::vector<Point2D> Point2D::getRandomPoints(int n) {
    std::vector<Point2D> points;

    for(int i = 0; i < n; ++i) {
        points.emplace_back(Engine::getInstance()->getRandomX(), Engine::getInstance()->getRandomY());
    }

    return points;
}

Point2D *Point2D::clone() {
    return new Point2D(this->x, this->y);
}

void Point2D::shift(Point2D point2D) {
    x = x + point2D.getX();
    y = y + point2D.getY();
}

void Point2D::rotate(double angle, Point2D point2D) {
    int x0 = point2D.getX();
    int y0 = point2D.getY();
    auto newX = x0 + (x - x0) * cos(angle) - (y - y0) * sin(angle);
    auto newY = y0 + (x - x0) * sin(angle) + (y - y0) * cos(angle);
    set(static_cast<int>(newX), static_cast<int>(newY));
}

void Point2D::scale(double factor, Point2D center) {
    //x = static_cast<int>(x * factor);
    //y = static_cast<int>(y * factor);
    auto newX = x * factor + (1 - factor) * center.getX();
    auto newY = y * factor + (1 - factor) * center.getY();
    set(static_cast<int>(newX), static_cast<int>(newY));
}

