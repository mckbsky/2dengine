#include <Primitive.hpp>

Primitive::Primitive(){
    this->color = Color(1.0,1.0,1.0);
}

void Primitive::setColor(Color color) {
    this->color = color;
}

int Primitive::getColor() {
    return this->color.getColor();
}

