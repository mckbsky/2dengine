#include <Engine.hpp>

BitmapHandler::BitmapHandler(int width,int height){
    this->width = width;
    this->height = height;

    this->bitmap=create_bitmap(width,height);
    if(!bitmap) {
        Engine::printError("Error creating bitmap");
    }

    clear_bitmap(bitmap);
}

BitmapHandler::BitmapHandler(int color_depth,int width, int height){
    this->bitmap=create_bitmap_ex(color_depth, width, height);
}

BitmapHandler::BitmapHandler(BITMAP* parentBitmap, Point2D* point, int width, int height){
    this->bitmap=create_sub_bitmap(parentBitmap, point->getX(), point->getY(), width, height);
}

void BitmapHandler::loadBitmap(const char* filename, RGB* palette){
    this->bitmap=load_bitmap(filename,palette);
    if(!bitmap) {
        Engine::getInstance()->printError("Cant load bitmap");
    }
}

void BitmapHandler::saveBitmap(const char* filename, const RGB* palette){
    save_bitmap(filename, this->bitmap, palette);
}

void BitmapHandler::copyArea(BITMAP* destination, Point2D sourcePoint,Point2D destinationPoint,int width, int height){
    blit(this->bitmap, destination, sourcePoint.getX(), sourcePoint.getY(), destinationPoint.getX(),
         destinationPoint.getY(), width, height);
}

BitmapHandler::~BitmapHandler(){
    destroy_bitmap(this->bitmap);
}

BITMAP *BitmapHandler::getBitmap() {
    return this->bitmap;
}

int BitmapHandler::getWidth() {
    return width;
}

int BitmapHandler::getHeight() {
    return height;
}

void BitmapHandler::setWidthHeight(int width, int height) {
    this->width = width;
    this->height = height;
}
