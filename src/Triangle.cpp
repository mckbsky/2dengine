#include "Triangle.hpp"

Triangle::Triangle(Point2D* a, Point2D* b, Point2D* c)
    : Primitive(), a(a), b(b), c(c) {}


void Triangle::draw(BITMAP *bitmap) {
    line(bitmap, a->getX(), a->getY(), b->getX(), b->getY(), color.getColor());
    line(bitmap, b->getX(), b->getY(), c->getX(), c->getY(), color.getColor());
    line(bitmap, c->getX(), c->getY(), a->getX(), a->getY(), color.getColor());
}


