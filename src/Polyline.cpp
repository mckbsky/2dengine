#include "Polyline.hpp"
#include <Color.hpp>
#include "Engine.hpp"


Polyline::Polyline(std::vector <Point2D*> points){
    this->points=points;
}

std::vector <Point2D*> Polyline::getPoints(){
    return this->points;
}

void Polyline::draw(BITMAP *bitmap){
    auto colorType = rand() % KHAKI;
    Color color = Color(static_cast<ColorType>(colorType));
    for (unsigned int i=0;i<Polyline::getPoints().size()-1;i++){
        line(bitmap, points[i]->getX(), points[i]->getY(), points[i+1]->getX(), points[i+1]->getY(), color.getColor());
    }
}

void Polyline::drawCl(BITMAP *bitmap){
    auto colorType = rand() % KHAKI;
    Color color = Color(static_cast<ColorType>(colorType));
    for (unsigned int i=0;i<Polyline::getPoints().size();i++){
        line(bitmap, points[i]->getX(), points[i]->getY(), points[(i+1)%Polyline::getPoints().size()]->getX(), points[(i+1)%Polyline::getPoints().size()]->getY(), color.getColor());
    }

}
