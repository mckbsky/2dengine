#include <Scene.hpp>
#include <InitBitmask.hpp>
#include <SpriteObject.hpp>

Engine::Engine() = default;

Engine* Engine::instance = nullptr;

Engine* Engine::getInstance() {
    if(!instance) {
        instance = new Engine();
    }
    return instance;
}

int Engine::MAX_X = 0;
int Engine::MAX_Y = 0;

void Engine::init(int bitmask) {
    init(GFX_AUTODETECT_WINDOWED, 640, 480, bitmask);
}

void Engine::init(int gfxMode, int resolutionX, int resolutionY, int bitmask) {
    allegro_init();
    set_gfx_mode(gfxMode, resolutionX, resolutionY, 0, 0);
    set_color_depth(32);
    handleBitmask(bitmask);
    install_timer();
    Engine::MAX_X = screen->w;
    Engine::MAX_Y = screen->h;

    bitmapHandler = new BitmapHandler(screen->w, screen->h);
}

void Engine::handleBitmask(int bitmask) {
    if(bitmask & KEYBOARD) {
        install_keyboard();
        set_keyboard_rate(0, 0);
        keyboard = new Keyboard();
    }
    if(bitmask & MOUSE) {
        install_mouse();
        mouse = new Mouse();
    }
}

void Engine::mainLoop() {
    auto *scene = new Scene(this);
    auto spriteObject = new SpriteObject("gfx/menu.bmp", 800, 600);

    while(true) {
        doubleBufferClear(Color(ColorType::BLUE));
        spriteObject->drawSprite();
        doubleBufferDraw();

        if(keyboard->isEsc()) {
            break;
        }
        if(keyboard->isOne()) {
            scene->drawPrimitives();
        }
        if(keyboard->isTwo()) {
            scene->drawIncrementalAlgorithm();
        }
        if(keyboard->isThree()) {
            scene->drawViewport();
        }
        if(keyboard->isFour()) {
            scene->drawCircles();
        }
        if(keyboard->isFive()) {
            scene->drawBoundingBox();
        }
        if(keyboard->isSix()) {
            scene->drawBitmaps();
        }
        if(keyboard->isSeven()) {
            scene->drawPolylines();
        }

        rest(Engine::MILISECONDS);

    }

    delete spriteObject;
}

void Engine::clear() {
    clear_bitmap(screen);
}


void Engine::clear(Color color) {
    clear_to_color(screen, color.getColor());
}

void Engine::printError(const char *message) {
    allegro_message("%s",message);
}

void Engine::close() {
    if(keyboard != nullptr) {
        remove_keyboard();
        delete(keyboard);
    }
    if(mouse != nullptr) {
        remove_mouse();
        delete(mouse);
    }
    remove_timer();
    allegro_exit();
}

Keyboard *Engine::getKeyboard() {
    return this->keyboard;
}

Mouse *Engine::getMouse() {
    return this->mouse;
}

int Engine::getRandomX() {
    std::random_device randomDevice;
    std::mt19937 mt(randomDevice());
    std::uniform_int_distribution<int> uniformDistributionX(0, Engine::MAX_X);
    return uniformDistributionX(mt);
}

int Engine::getRandomY() {
    std::random_device randomDevice;
    std::mt19937 mt(randomDevice());
    std::uniform_int_distribution<int> uniformDistributionY(0, Engine::MAX_Y);
    return uniformDistributionY(mt);
}

void Engine::doubleBufferDraw() {
    this->bitmapHandler->copyArea(screen, {0, 0}, {0, 0}, Engine::MAX_X, Engine::MAX_Y);
}

BITMAP *Engine::getBitmap() {
    return this->bitmapHandler->getBitmap();
}

void Engine::doubleBufferClear(Color color) {
    clear_to_color(this->bitmapHandler->getBitmap(), color.getColor());
}

void Engine::doubleBufferClear() {
    clear_bitmap(this->bitmapHandler->getBitmap());
}


