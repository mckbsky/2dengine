
#include <Point2D.hpp>
#include <BoundingBox.hpp>

Point2D* BoundingBox::getPointA() {
    return this->pointA;
}

Point2D* BoundingBox::getPointB() {
    return this->pointB;
}

void BoundingBox::draw(BITMAP* bitmap) {
    rect(bitmap, pointA->getX(), pointA->getY(), pointB->getX(), pointB->getY(), Color(ColorType::BLACK).getColor());
}

bool BoundingBox::checkCollision(BoundingBox *boundingBox) {
    return this->getPointA()->getX() < boundingBox->getPointB()->getX() &&
           this->getPointB()->getX() > boundingBox->getPointA()->getX() &&
           this->getPointA()->getY() < boundingBox->getPointB()->getY() &&
           this->getPointB()->getY() > boundingBox->getPointA()->getY();
}

BoundingBox::BoundingBox(Point2D* pointA, Point2D* pointB)
    : pointA(pointA), pointB(pointB){}

BoundingBox *BoundingBox::clone() {
    return new BoundingBox(pointA->clone(), pointB->clone());
}

void BoundingBox::shift(Point2D point2D) {
    pointA->shift(point2D);
    pointB->shift(point2D);
}

