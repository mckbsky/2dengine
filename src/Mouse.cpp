#include <Mouse.hpp>
#include <MouseType.hpp>

Mouse::Mouse(){
    show_os_cursor(1);
}

Point2D Mouse::getPosition(){
   return {mouse_x,mouse_y};
}

bool Mouse::isLeftButton(){
    return static_cast<bool>(mouse_b & LEFT_BUTTON);
}

bool Mouse::isRightButton(){
    return static_cast<bool>(mouse_b & RIGHT_BUTTON);
}
