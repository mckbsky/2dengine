#include <Point2D.hpp>
#include <Viewport.hpp>
#include <Engine.hpp>

Viewport::Viewport(Point2D *a, Point2D *b)
    : Primitive(), a(a), b(b) {}

void Viewport::draw(BITMAP* bitmap) {
    rect(bitmap, a->getX(), a->getY(), b->getX(), b->getY(), color.getColor());
}

Point2D *Viewport::getPointA() {
    return this->a;
}

Point2D *Viewport::getPointB() {
    return this->b;
}

bool Viewport::containing(Point2D point) {
    return point.getX() > getPointA()->getX() && point.getX() < getPointB()->getX() &&
        point.getY() > getPointA()->getY() && point.getY() < getPointB()->getY();
}

std::vector<LineSegment *> Viewport::intersectLine(LineSegment *lineSegment) {
    std::vector<LineSegment*> segments;

    if(bothPointsInsideViewport(lineSegment)) {
        segments.push_back(new LineSegment(new Point2D(lineSegment->getPointA()->getX(), lineSegment->getPointA()->getY()),
                                           new Point2D(lineSegment->getPointB()->getX(), lineSegment->getPointB()->getY())));
    }
    else if(onePointInsideViewport(lineSegment)) {
        std::vector<Point2D*> point = findIntersectionPointWithViewport(lineSegment);

        if(containing(*lineSegment->getPointA())) {
            segments.emplace_back(new LineSegment(lineSegment->getPointA(), point.at(0)));
            segments.emplace_back(new LineSegment(point.at(0), lineSegment->getPointB()));
        }
        else {
            segments.emplace_back(new LineSegment(lineSegment->getPointB(), point.at(0)));
            segments.emplace_back(new LineSegment(point.at(0), lineSegment->getPointA()));
        }
    }
    else {
        std::vector<Point2D*> points = findIntersectionPointWithViewport(lineSegment);
        if(points.size() == 2) {
            segments.emplace_back(new LineSegment(lineSegment->getPointA(), points[0]));
            segments.emplace_back(new LineSegment(lineSegment->getPointB(), points[1]));
            segments.emplace_back(new LineSegment(points[0], points[1]));

        }
    }

    return segments;
}

bool Viewport::bothPointsInsideViewport(LineSegment *lineSegment) {
    return containing(*lineSegment->getPointA()) && containing(*lineSegment->getPointB());
}

bool Viewport::onePointInsideViewport(LineSegment *lineSegment) {
    return (containing(*lineSegment->getPointA()) && !containing(*lineSegment->getPointB()))
           || (!containing(*lineSegment->getPointA()) && containing(*lineSegment->getPointB()));
}

std::vector<Point2D*> Viewport::findIntersectionPointWithViewport(LineSegment *lineSegment) {
    std::vector<LineSegment*> sides = getViewportSides();
    std::vector<Point2D*> points;

    for(LineSegment *segment : sides) {
        Point2D* point = getIntersectPoint(segment, lineSegment);
        if(point != nullptr) {
           points.push_back(point);
        }
    }

    sides.clear();
    return points;
}

Point2D *Viewport::getIntersectPoint(LineSegment *l1, LineSegment *l2) {
    float x1 = l1->getPointA()->getX();
    float x2 = l1->getPointB()->getX();
    float x3 = l2->getPointA()->getX();
    float x4 = l2->getPointB()->getX();


    float y1 = l1->getPointA()->getY();
    float y2 = l1->getPointB()->getY();
    float y3 = l2->getPointA()->getY();
    float y4 = l2->getPointB()->getY();

    float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

    if (d == 0) {
        return nullptr;
    }

    float pre = (x1*y2 - y1*x2), post = (x3*y4 - y3*x4);
    float x = ( pre * (x3 - x4) - (x1 - x2) * post ) / d;
    float y = ( pre * (y3 - y4) - (y1 - y2) * post ) / d;

    if (x < std::min(x1, x2) || x > std::max(x1, x2) || x < std::min(x3, x4) || x > std::max(x3, x4)) {
        return nullptr;
    }
    if (y < std::min(y1, y2) || y > std::max(y1, y2) || y < std::min(y3, y4) || y > std::max(y3, y4)) {
        return nullptr;
    }

    return new Point2D(static_cast<int>(x), static_cast<int>(y));
}

std::vector<LineSegment *> Viewport::getViewportSides() {
    std::vector<LineSegment*> sides;

    sides.emplace_back(new LineSegment(getPointA()->clone(), new Point2D(getPointB()->getX(), getPointA()->getY())));
    sides.emplace_back(new LineSegment(new Point2D(getPointB()->getX(), getPointA()->getY()), getPointB()->clone()));
    sides.emplace_back(new LineSegment(getPointB()->clone(), new Point2D(getPointA()->getX(), getPointB()->getY())));
    sides.emplace_back(new LineSegment(new Point2D(getPointA()->getX(), getPointB()->getY()), getPointA()->clone()));

    return sides;
}

