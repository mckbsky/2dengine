#include <Engine.hpp>
#include "SpriteObject.hpp"

void SpriteObject::drawSprite() {
    draw_sprite(Engine::getInstance()->getBitmap(), this->bitmapHandler->getBitmap(), position->getX(), position->getY());
}

SpriteObject::SpriteObject(const char *filename, int width, int height) {
    bitmapHandler = new BitmapHandler(width, height);
    bitmapHandler->loadBitmap(filename, default_palette);
    position = new Point2D(0, 0);
}

Point2D *SpriteObject::getPosition() {
    return position;
}

void SpriteObject::strechSprite(const double stretchFactor) {
    bitmapHandler->setWidthHeight(static_cast<int>(bitmapHandler->getWidth() * stretchFactor),
                                  static_cast<int>(bitmapHandler->getHeight() * stretchFactor));
    stretch_sprite(Engine::getInstance()->getBitmap(), bitmapHandler->getBitmap(), position->getX(), position->getY(),
                   bitmapHandler->getWidth(), bitmapHandler->getHeight());
}

void SpriteObject::rotateSprite() {
    rotate_sprite(Engine::getInstance()->getBitmap(), bitmapHandler->getBitmap(), position->getX(), position->getY(),
    ftofix(rotationAngle));
}

void SpriteObject::setRotation(double rotation) {
    rotationAngle += rotation;
}

void SpriteObject::verticalFlipSprite() {
    draw_sprite_v_flip(Engine::getInstance()->getBitmap(), bitmapHandler->getBitmap(), position->getX(), position->getY());
}

void SpriteObject::horizontalFlipSprite() {
    draw_sprite_h_flip(Engine::getInstance()->getBitmap(), bitmapHandler->getBitmap(), position->getX(), position->getY());
}

void SpriteObject::flipSprite() {
    draw_sprite_vh_flip(Engine::getInstance()->getBitmap(), bitmapHandler->getBitmap(), position->getX(), position->getY());
}

