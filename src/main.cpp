
#include <InitBitmask.hpp>
#include <Engine.hpp>

int main() {

    Engine::getInstance()->init(GFX_AUTODETECT_WINDOWED, 800, 600, KEYBOARD | MOUSE);
    //engine.init(KEYBOARD | MOUSE);

    Engine::getInstance()->mainLoop();
    Engine::getInstance()->close();

    return 0;
}
