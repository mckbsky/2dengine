#include "Circle.hpp"
#include <allegro.h>
#include <Engine.hpp>

Circle::Circle(int radius,Point2D* center)
    :Primitive(), radius(radius), center(center) {
        this->DELTA = 1.0/radius;
        this->boundingBox = new BoundingBox(new Point2D(center->getX() - radius, center->getY() - radius),
                                            new Point2D(center->getX() + radius, center->getY() + radius));
    }

void Circle::draw(BITMAP* bitmap){
    double alpha=0;
    while(alpha<2*PI){
        auto x= static_cast<int>(this->center->getX() + radius * cos(alpha));
        auto y= static_cast<int>(this->center->getY() + radius * sin(alpha));
        putpixel(bitmap, x, y, color.getColor());
        alpha+=DELTA;
    }
}

void Circle::draw(BITMAP* bitmap, CircleType circleType){
    if(circleType == QUADRUPLE)
        this->drawQuadruple(bitmap);
    else if(circleType == OCTAL)
        this->drawOctal(bitmap);
    else
        Engine::getInstance()->printError("Niepoprawny CircleType");
}

void Circle::drawQuadruple(BITMAP* bitmap){
    double alpha=0.0;
    while(alpha<PI/2){
        auto x= static_cast<int>(radius * cos(alpha));
        auto y= static_cast<int>(radius * sin(alpha));
        putpixel(bitmap, this->center->getX()+x, this->center->getY()+y, Color(ColorType::BLUE).getColor());
        putpixel(bitmap, this->center->getX()+x, this->center->getY()-y, Color(ColorType::GREEN).getColor());
        putpixel(bitmap, this->center->getX()-x, this->center->getY()-y, Color(ColorType::MAGENTA).getColor());
        putpixel(bitmap, this->center->getX()-x, this->center->getY()+y, Color(ColorType::YELLOW).getColor());
        alpha+=DELTA;
    }
}

void Circle::drawOctal(BITMAP* bitmap){
    double alpha=0.0;
    while(alpha<PI/4){
        auto x= static_cast<int>(radius * cos(alpha));
        auto y= static_cast<int>(radius * sin(alpha));
        putpixel(bitmap, this->center->getX()+x, this->center->getY()+y, Color(ColorType::BLUE).getColor());
        putpixel(bitmap, this->center->getX()+y, this->center->getY()+x, Color(ColorType::GREEN).getColor());
        putpixel(bitmap, this->center->getX()+x, this->center->getY()-y, Color(ColorType::MAGENTA).getColor());
        putpixel(bitmap, this->center->getX()+y, this->center->getY()-x, Color(ColorType::YELLOW).getColor());
        putpixel(bitmap, this->center->getX()-x, this->center->getY()-y, Color(ColorType::WHITE).getColor());
        putpixel(bitmap, this->center->getX()-y, this->center->getY()-x, Color(ColorType::CYAN).getColor());
        putpixel(bitmap, this->center->getX()-x, this->center->getY()+y, Color(ColorType::RED).getColor());
        putpixel(bitmap, this->center->getX()-y, this->center->getY()+x, Color(ColorType::SILVER).getColor());
        alpha+=DELTA;
    }
}

bool Circle::contains(Point2D point){
    return sqrt(pow(point.getX()-center->getX(),2.0)+pow(point.getY()-center->getY(),2.0))<radius;
}

void Circle::fill(BITMAP* bitmap, Point2D point,Color fillColor,Color backgroundColor){
    int pixel = getpixel(bitmap,point.getX(),point.getY());
    Color pointColor= Color(getr(pixel),getg(pixel),getb(pixel));
    if(pointColor.getColor()==fillColor.getColor())
        return;
    else
        if(pointColor.getColor()==backgroundColor.getColor()){
            putpixel(bitmap,point.getX(),point.getY(),fillColor.getColor());
            fill(bitmap, Point2D(point.getX(),point.getY()-1),fillColor,backgroundColor);
            fill(bitmap, Point2D(point.getX(),point.getY()+1),fillColor,backgroundColor);
            fill(bitmap, Point2D(point.getX()-1,point.getY()),fillColor,backgroundColor);
            fill(bitmap, Point2D(point.getX()+1,point.getY()),fillColor,backgroundColor);
        }
}


BoundingBox *Circle::getBoundingBox() {
    return this->boundingBox;
}

Point2D *Circle::getCenter() {
    return this->center;
}
