#include <Engine.hpp>

Color::Color(int red, int green, int blue) {
    this->color = makecol(red, green, blue);
}

Color::Color(double red, double green, double blue) {
    this->color = makecol(static_cast<int>(red * 255), static_cast<int>(green * 255), static_cast<int>(blue * 255));
}

Color::Color(ColorType colorType) {
    switch (colorType) {
        case RED:
            this->color = makecol(255, 0, 0);
            break;
        case GREEN:
            this->color = makecol(0, 255, 0);
            break;
        case BLUE:
            this->color = makecol(0, 0, 255);
            break;
        case BLACK:
            this->color = makecol(0, 0, 0);
            break;
        case YELLOW:
            this->color = makecol(255, 255, 0);
            break;
        case MAGENTA:
            this->color = makecol(255, 0, 255);
            break;
        case WHITE:
            this->color = makecol(255, 255, 255);
            break;
        case SILVER:
            this->color = makecol(192, 192, 192);
            break;
        case CYAN:
            this->color = makecol(0, 255, 255);
            break;
        case INDIGO:
            this->color = makecol(75, 0, 130);
            break;
        case CRIMSON:
            this->color = makecol(60, 0, 0);
            break;
        case BISQUE:
            this->color = makecol(255, 228, 196);
            break;
        case OLIVE:
            this->color = makecol(128, 128, 0);
            break;
        case KHAKI:
            this->color = makecol(240, 230, 140);
            break;
        default:
            Engine::getInstance()->printError("Niepoprawnie wprowadzony kolor");
    }
}

int Color::getColor() {
    return this->color;
}
