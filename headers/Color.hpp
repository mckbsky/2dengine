#ifndef INC_2DENGINE_COLOR_HPP
#define INC_2DENGINE_COLOR_HPP

#include "ColorType.hpp"

class Color {
private:
   int color;
public:
    Color(int, int, int);
    Color(double,double,double);

    explicit Color(ColorType);
    int getColor();
};

#endif
