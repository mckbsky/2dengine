#ifndef INC_2DENGINE_BOUNDINGBOX_HPP
#define INC_2DENGINE_BOUNDINGBOX_HPP

class BoundingBox : public Primitive {
private:
    Point2D* pointA;
    Point2D* pointB;

public:
    BoundingBox(Point2D*, Point2D*);
    Point2D* getPointA();
    Point2D* getPointB();
    void draw(BITMAP*) override;
    bool checkCollision(BoundingBox*);
    void shift(Point2D);
    BoundingBox* clone();
};

#endif
