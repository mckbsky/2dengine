#ifndef RECTANGLE_HPP_INCLUDED
#define RECTANGLE_HPP_INCLUDED

#include "BoundingBox.hpp"
#include "Primitive.hpp"
#include "Point2D.hpp"

class Rectangle : public Primitive {
private:
    Point2D *a;
    Point2D *b;
    BoundingBox* boundingBox;

public:
    Rectangle(Point2D*,Point2D*);
    void draw(BITMAP*) override;
    BoundingBox* getBoundingBox();
    Point2D* getPointA();
    Point2D* getPointB();
    void shift(Point2D);
    void rotate(double angle);
    void scale(double factor);
};

#endif
