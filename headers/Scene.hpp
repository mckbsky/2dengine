#ifndef INC_2DENGINE_SCENE_HPP
#define INC_2DENGINE_SCENE_HPP

#include "Engine.hpp"

class Scene {
private:
    Engine* engine;

public:
    explicit Scene(Engine *);
    void drawPrimitives();
    void drawIncrementalAlgorithm();
    void drawViewport();
    void drawCircles();
    void drawBitmaps();
    void drawPolylines();

    void drawBoundingBox();
};

#endif
