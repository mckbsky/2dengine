#ifndef INC_2DENGINE_KEYBOARD_HPP
#define INC_2DENGINE_KEYBOARD_HPP

class Keyboard {
public:
    const static int MASK = 0xff;

    int handle();
    bool isEsc();
    bool isRightArrow();
    bool isLeftArrow();
    bool isUpArrow();
    bool isDownArrow();
    bool isOne();
    bool isTwo();
    bool isThree();
    bool isFour();
    bool isFive();
    bool isSix();
    bool isSeven();
    bool isT();
    bool isL();
    bool isR();
    bool isC();
    bool isO();
    bool isF();
    bool isB();
    bool isQ();
    bool isV();
    bool isH();
    bool isX();
    bool isComma();
    bool isDot();
    bool isPlus();
    bool isMinus();

    bool isBackspace();

};


#endif
