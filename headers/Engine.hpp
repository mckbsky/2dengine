#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <string>
#include <random>
#include <iostream>
#include "Keyboard.hpp"
#include "Mouse.hpp"
#include "BitmapHandler.hpp"
#include "BoundingBox.hpp"

class Engine {
private:
    Keyboard *keyboard;
    Mouse *mouse;

    Engine();

    void handleBitmask(int);

    static Engine *instance;

public:
    constexpr static int MILISECONDS = 1000 / 60;
    constexpr static int DELAY = 1000 / 15;
    static int MAX_X;
    static int MAX_Y;
    BitmapHandler *bitmapHandler;

    static Engine *getInstance();

    void init(int);

    void init(int, int, int, int);

    void mainLoop();

    static void printError(const char *);

    void clear();

    void clear(Color);

    void doubleBufferClear();

    void doubleBufferClear(Color);

    void close();

    Keyboard *getKeyboard();

    Mouse *getMouse();

    int getRandomX();

    int getRandomY();

    void doubleBufferDraw();

    BITMAP *getBitmap();

    template<typename T1, typename T2>
    bool move(T1 *quadrangle, int delta, std::vector<T2> obstacles) {
        bool returnValue = false;
        Point2D point2D(0, 0);

        if (keyboard->isUpArrow()) {
            point2D.set(0, -delta);
            returnValue = true;
        }
        if (keyboard->isDownArrow()) {
            point2D.set(0, delta);
            returnValue = true;
        }
        if (keyboard->isRightArrow()) {
            point2D.set(delta, 0);
            returnValue = true;
        }
        if (keyboard->isLeftArrow()) {
            point2D.set(-delta, 0);
            returnValue = true;
        }

        if(returnValue) {
            returnValue = moveDirection(point2D, quadrangle, obstacles);
        }

        return returnValue;
    }

    template<typename T1, typename T2>
    bool moveDirection(Point2D point2D, T1 *quadrangle, std::vector<T2> obstacles) {
        if (!obstacles.empty()) {
            if (quadrangle->getBoundingBox() != nullptr) {
                BoundingBox *boundingBox = quadrangle->getBoundingBox()->clone();
                boundingBox->shift(point2D);

                for (auto &obstacle : obstacles) {
                    if (boundingBox->checkCollision(obstacle->getBoundingBox())) {
                        return false;
                    }
                }

                quadrangle->getBoundingBox()->shift(point2D);
            }
        }

        quadrangle->shift(point2D);
        return true;
    }

    template<typename T1>
    bool rotate(T1 *quadrangle) {
        bool returnValue = false;

        if(keyboard->isComma()) {
            quadrangle->rotate(M_PI / 90);
            returnValue = true;
        }
        if(keyboard->isDot()) {
            quadrangle->rotate(M_PI / 90);
            returnValue = true;
        }

        return returnValue;
    }

    template<typename T1>
    bool scale(T1* quadrangle) {
        bool returnValue = false;

        if(keyboard->isMinus()) {
            quadrangle->scale(0.95);
            returnValue = true;
        }

        if(keyboard->isPlus()) {
            quadrangle->scale(1.05);
            returnValue = true;
        }

        return returnValue;
    }
};

#endif
