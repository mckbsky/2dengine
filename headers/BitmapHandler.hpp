#ifndef BITMAPHANDLER_HPP_INCLUDED
#define BITMAPHANDLER_HPP_INCLUDED

class BitmapHandler{
private:
    Point2D* startPoint;
    int width;
    int height;
    BITMAP* bitmap;

public:
    BitmapHandler(int,int);
    BitmapHandler(int,int,int);
    BitmapHandler(BITMAP*,Point2D*,int,int);
    int getWidth();
    int getHeight();
    void setWidthHeight(int, int);
    void loadBitmap(const char*, RGB*);
    void saveBitmap(const char*, const RGB*);
    void copyArea(BITMAP*, Point2D, Point2D, int width, int height);
    BITMAP* getBitmap();
    ~BitmapHandler();
};

#endif
