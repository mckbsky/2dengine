#ifndef SPRITEOBJECT_HPP_INCLUDED
#define SPRITEOBJECT_HPP_INCLUDED

#include "BitmapHandler.hpp"
#include "Point2D.hpp"

class SpriteObject{
private:
    BitmapHandler* bitmapHandler;
    Point2D* position;
    double rotationAngle = 0;

public:
    explicit SpriteObject(const char*, int, int);
    void drawSprite();
    void strechSprite(double);
    void rotateSprite();
    void verticalFlipSprite();
    void horizontalFlipSprite();
    void flipSprite();
    Point2D* getPosition();
    void setRotation(double);

};

#endif
