#ifndef INIT_BITMASK_HPP
#define INIT_BITMASK_HPP

enum InitBitmask {
  KEYBOARD = 1,
  MOUSE = 2,
};

#endif
