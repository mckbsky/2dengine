#ifndef MOUSE_HPP_INCLUDED
#define MOUSE_HPP_INCLUDED

#include "Point2D.hpp"

class Mouse{
public:
    Mouse();
    Point2D getPosition();
    bool isLeftButton();
    bool isRightButton();


};

#endif
