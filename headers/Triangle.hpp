#ifndef INC_2DENGINE_TRIANGLE_HPP
#define INC_2DENGINE_TRIANGLE_HPP

#include "Primitive.hpp"
#include "Point2D.hpp"

class Triangle : public Primitive {
private:
    Point2D* a;
    Point2D* b;
    Point2D* c;

public:
    Triangle(Point2D*, Point2D*, Point2D*);
    void draw(BITMAP*) override;
};


#endif
