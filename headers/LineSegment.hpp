#ifndef INC_2DENGINE_LINESEGMENT_HPP
#define INC_2DENGINE_LINESEGMENT_HPP

#include "Primitive.hpp"
#include "Point2D.hpp"

class LineSegment : public Primitive {
private:
    Point2D *a;
    Point2D *b;

public:
    LineSegment(Point2D*, Point2D*);
    static std::vector<LineSegment> getRandomLines(int);

    void draw(BITMAP*) override;
    void swapPoints();
    Point2D* getPointA();
    Point2D* getPointB();
    LineSegment* clone();

};

#endif
