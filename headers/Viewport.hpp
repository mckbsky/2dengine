#ifndef INC_2DENGINE_VIEWPORT_HPP
#define INC_2DENGINE_VIEWPORT_HPP

#include "Point2D.hpp"
#include "Color.hpp"
#include "Primitive.hpp"
#include "LineSegment.hpp"
#include <list>

class Viewport : public Primitive{
private:
    Point2D *a;
    Point2D *b;

    bool bothPointsInsideViewport(LineSegment *pSegment);
    bool onePointInsideViewport(LineSegment *pSegment);
    std::vector<Point2D*> findIntersectionPointWithViewport(LineSegment *pSegment);
    Point2D *getIntersectPoint(LineSegment *, LineSegment *);
    std::vector<LineSegment*> getViewportSides();

public:
    Viewport(Point2D*, Point2D*);
    void draw(BITMAP *) override;
    Point2D* getPointA();
    Point2D* getPointB();
    bool containing(Point2D);
    std::vector<LineSegment*> intersectLine(LineSegment *lineSegment);

};

#endif
