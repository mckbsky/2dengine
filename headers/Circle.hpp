#ifndef CIRCLE_HPP_INCLUDED
#define CIRCLE_HPP_INCLUDED

#include <cmath>
#include "Primitive.hpp"
#include "Point2D.hpp"
#include "BoundingBox.hpp"
#include "CircleType.hpp"

class Circle : public Primitive{
private:
    int radius;
    Point2D *center;
    const double PI = M_PI;
    double DELTA;
    BoundingBox* boundingBox;


public:
    Circle(int,Point2D*);
    void draw(BITMAP*) override;
    void draw(BITMAP*, CircleType);
    void drawQuadruple(BITMAP*);
    void drawOctal(BITMAP*);
    Point2D* getCenter();
    bool contains(Point2D);
    void fill(BITMAP*, Point2D, Color, Color);
    BoundingBox* getBoundingBox();
};


#endif
