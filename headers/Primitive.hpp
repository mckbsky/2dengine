#ifndef PRIMITIVE_HPP_INCLUDED
#define PRIMITIVE_HPP_INCLUDED

#include <allegro.h>
#include "Color.hpp"

class Primitive {
protected:
    Color color = Color(ColorType::WHITE);

public:
    Primitive();
    virtual void draw(BITMAP* bitmap) = 0;
    void setColor(Color);
    int getColor();
};

#endif
