#ifndef POLYLINE_HPP_INCLUDED
#define POLYLINE_HPP_INCLUDED
#include "header.hpp"
#include "Point2D.hpp"


class Polyline {
private:
    std::vector <Point2D*> points;

public:
    Polyline(std::vector <Point2D*>);
    void draw(BITMAP *);
    void drawCl(BITMAP *);
    std::vector <Point2D*> getPoints();
};

#endif
