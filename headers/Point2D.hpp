#ifndef INC_2DENGINE_POINT2D_HPP
#define INC_2DENGINE_POINT2D_HPP

#include <vector>
#include "Color.hpp"
#include "Primitive.hpp"

class Point2D : public Primitive {
private:
    int x;
    int y;

public:
    Point2D(int, int);
    int getX();
    int getY();
    void draw(BITMAP*) override;
    void setX(int);
    void setY(int);
    void set(int, int);
    static std::vector<Point2D> getRandomPoints(int);
    Point2D* clone();
    void shift(Point2D);
    void rotate(double, Point2D);
    void scale(double, Point2D);
};

#endif
